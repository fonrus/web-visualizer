const sharp = require("sharp");
const mkdir = require("mkdirp");
const async = require("async");
const fs = require("graceful-fs");
const progress = require("progress");

const ensureDir = function(dir) {
    return new Promise(function(resolve, reject) {
        mkdir(dir, function(err) {
            if (err) reject(err);
            else {
                resolve();
            }
        });
    });
};

const getPath = function(path, z, x, y) {
    return path.replace("{z}", z).replace("{x}", x).replace("{y}", y);
};

const planImage = async function(imageFile, saveLocation) {
    const plans = [];
    const img = await sharp(imageFile).limitInputPixels(536870913);
    img.setMaxListeners(0);
    const meta = await img.clone().metadata();
    const dim = Math.max(meta.width, meta.height);
    let scale = 0;
    let size = 1;
    while (256 * (size = Math.pow(2, scale)) <= dim) {
        const originSize = dim / size;
        for (let x = 0; x < meta.width / originSize; x++) {
            for (let y = 0; y < meta.height / originSize; y++) {
                const file = getPath(saveLocation, scale, x, y);
                let left = x * originSize;
                let top = y * originSize;
                let width = Math.min(meta.width, originSize);
                let height = Math.min(meta.height, originSize);
                plans.push({
                    dims: {
                        left: left,
                        top: top,
                        width: width,
                        height: height
                    },
                    img: img,
                    width: 256 * (width / originSize),
                    height: 256 * (height / originSize),
                    file: file
                });
            }
        }
        scale++;
    }
    return plans;
};

process.setMaxListeners(0);
process.on('unhandledRejection', error => {
    console.error('unhandledRejection', error);
});

const planDirs = function(plans) {
    const dirs = [];
    plans.forEach(plan => {
        const dir = plan.file.match(/(.*)[\/\\]/)[1] || '';
        if (dirs.indexOf(dir) < 0) dirs.push(dir);
    });
    return dirs;
};

const executePlan = function(plan) {
    return new Promise(async function(resolve, reject) {
        plan.img
            .clone()
            .extract({
                left: plan.dims.left,
                top: plan.dims.top,
                width: plan.dims.width,
                height: plan.dims.height
            })
            .resize(plan.width, plan.height)
            .png()
            .toFile(plan.file, function(err, info) {
                if (err) return reject(err);
                resolve();
            });
    });
};

const allProgress = function(proms, progress_cb) {
    let d = 0;
    progress_cb(0, proms.length, 0);
    proms.forEach((p) => {
        p.then(() => {
            d++;
            progress_cb(d, proms.length, (d * 100) / proms.length);
        });
    });
    return Promise.all(proms);
};

function doImage(input, output) {
    return new Promise(async(resolve, reject) => {
        let plans = [];
        plans = plans.concat(await planImage(input, output));
        await Promise.all(planDirs(plans).map(dir => {
            return ensureDir(dir);
        }));
        let perc = 0;
        await allProgress(plans.map(plan => {
            return executePlan(plan);
        }), (c, t, p) => {
            if (Math.floor(p) > perc) {
                perc = Math.floor(p);
                console.log(input, "-", perc);
            }
        });
        resolve();
    });
};

function addZoom(dir, maxZoom) {
    console.log("adding zoom", dir, "to", maxZoom);
    const zoomOp = function(zoom) {
        const zoomy = function(zoom, x, y) {
            return new Promise(async function(resolve, reject) {
                var imgBuf = sharp(dir + "/" + zoom + "/" + x + "_" + y + ".png").resize(512, 512, { interpolator: sharp.interpolator.nearest });
                await imgBuf.extract({ left: 0, top: 0, width: 256, height: 256 }).png({ compressionLevel: 9 }).toFile(dir + "/" + (zoom + 1) + "/" + (x * 2) + "_" + (y * 2) + ".png");
                await imgBuf.extract({ left: 256, top: 0, width: 256, height: 256 }).png({ compressionLevel: 9 }).toFile(dir + "/" + (zoom + 1) + "/" + (x * 2 + 1) + "_" + (y * 2) + ".png");
                await imgBuf.extract({ left: 0, top: 256, width: 256, height: 256 }).png({ compressionLevel: 9 }).toFile(dir + "/" + (zoom + 1) + "/" + (x * 2) + "_" + (y * 2 + 1) + ".png");
                await imgBuf.extract({ left: 256, top: 256, width: 256, height: 256 }).png({ compressionLevel: 9 }).toFile(dir + "/" + (zoom + 1) + "/" + (x * 2 + 1) + "_" + (y * 2 + 1) + ".png");
                resolve();
            });
        };
        return new Promise(function(resolve, reject) {
            fs.mkdirSync(dir + "/" + (zoom + 1));
            fs.readdir(dir + "/" + zoom, async function(err, items) {
                for (var i = 0; i < items.length; i++) {
                    var nums = items[i].match(/\d+/g);
                    var x = parseInt(nums[0]);
                    var y = parseInt(nums[1]);
                    await zoomy(zoom, x, y);
                }
                resolve();
            });
        });
    };
    return new Promise(function(resolve, reject) {
        fs.readdir(dir, async function(err, items) {
            var maxFoundZoom = -1;
            for (var i = 0; i < items.length; i++) {
                var nums = items[i].match(/\d+/g);
                var x = parseInt(nums[0]);
                maxFoundZoom = Math.max(x, maxFoundZoom);
            }
            for (var i = maxFoundZoom + 1; i <= maxZoom; i++) {
                console.log("performing zoom ", dir, "level", i);
                await zoomOp(i - 1);
            }
            resolve();
        });
    });
};

async function go() {
    console.log("Time to tile!");
    await doImage("./images/game_0.png", "./public/game/0/{z}/{x}_{y}.png");
    await addZoom("./public/game/0", 10);
    await doImage("./images/game_1.png", "./public/game/1/{z}/{x}_{y}.png");
    await addZoom("./public/game/1", 10);
    await doImage("./images/game_2.png", "./public/game/2/{z}/{x}_{y}.png");
    await addZoom("./public/game/2", 10);
    await doImage("./images/game_3.png", "./public/game/3/{z}/{x}_{y}.png");
    await addZoom("./public/game/3", 10);

    await doImage("./images/web_0.png", "./public/web/0/{z}/{x}_{y}.png");
    await addZoom("./public/web/0", 10);
    await doImage("./images/web_1.png", "./public/web/1/{z}/{x}_{y}.png");
    await addZoom("./public/web/1", 10);
    await doImage("./images/web_2.png", "./public/web/2/{z}/{x}_{y}.png");
    await addZoom("./public/web/2", 10);
    await doImage("./images/web_3.png", "./public/web/3/{z}/{x}_{y}.png");
    await addZoom("./public/web/3", 10);
    console.log("Done!");
};

go();