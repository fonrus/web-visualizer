## CI for map tile generation  
Files are expected in the format  
Game Map Files (2x2 per tile)  
`game_0.png`  
`game_1.png`  
`game_2.png`  
`game_3.png`  
  
Web Map Files (1x1 per tile)  
`web_0.png`  
`web_1.png`  
`web_2.png`  
`web_3.png`  
